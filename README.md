# Community Lores Requests for Comments

Community Lores Requests for Comments (CLRFCs) describes the project's direction and its policies, including programmatic access to documentation and policies regarding hosting content within the Community Lores GitLab SaaS namespace.

Once your first PR is merged, we may setup a bot in the futurethat helps out by automatically merging PRs to draft EIPs. For this to work, it has to be able to tell that you own the draft being edited. Make sure that the 'author' line of your EIP contains either your GitLab SaaS username or your email address inside `<triangular brackets>`. If you use your email address, that address must be the one publicly shown on your GitLab SaaS profile.

## Project Goal: The Why

The Community Lores Requests for Comments exists as a place for everybody, both the project maintainers, coordinators and the wider community, to share concrete proposals to improve the Community Lores project and the community at large.

Even through Community Lores is covered by Recap Time's Community Code of Conduct and other Community Policies, the project has its own security policy, privacy policy and ToS.

## Preferred Project Format

The canonical URL for a CLRFC that has achieved draft status at any point is at https://rfc.community-lores.gq/. For example, the canonical URL for CLRFC-1 is https://rfc.community-lores.gq/CLRFCS/clrfc-1.

Please consider anything which is not published on https://rfc.community-lores.gq/ as a working paper.

And please consider anything published at https://rfc.community-lores.gq/ with a status of "draft" as an incomplete draft.

## Contributing and Development Work

TODO

## License

CC0
